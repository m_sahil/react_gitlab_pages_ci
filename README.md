## React Gitlab Pages CI Demo

Demo Project for CI/CD pipeline using create-react-app template
URL: https://m_sahil.gitlab.io/react_gitlab_pages_ci/

#### To start

1. git clone https://gitlab.com/m_sahil/react_gitlab_pages_ci.git
2. cd react_gitlab_pages_ci
3. npm install
4. npm start
